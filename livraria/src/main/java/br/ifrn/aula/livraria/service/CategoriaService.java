package br.ifrn.aula.livraria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ifrn.aula.livraria.model.Categoria;
import br.ifrn.aula.livraria.repository.CategoriaRepository;

@Service
public class CategoriaService {
	@Autowired
	private CategoriaRepository repository;
	
	public  List<Categoria> findAll() {
		return repository.findAll();
	}
	
	public Categoria  findOne(Long id) {
        return repository.getOne(id);
    }
     
    public void save(Categoria cat) {
        repository.saveAndFlush(cat);
    }
     
    public void delete(Long id) {
        repository.deleteById(id);
    }
}

package br.ifrn.aula.livraria.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;

import br.ifrn.aula.livraria.service.UserService;

//@Configuration
public class UserConfig {
	
	//@Autowired
	public void configureGlobal(AuthenticationManagerBuilder builder, PasswordEncoder passwordEncoder, UserService userDetailsService) throws Exception {
		
		builder
			.userDetailsService(userDetailsService)
			.passwordEncoder(passwordEncoder);
	}
	
}	
package br.ifrn.aula.livraria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ifrn.aula.livraria.model.Autor;

@Repository
public interface AutorRepository extends JpaRepository<Autor,Long>{
	

	
}

package br.ifrn.aula.livraria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ifrn.aula.livraria.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long>{

}

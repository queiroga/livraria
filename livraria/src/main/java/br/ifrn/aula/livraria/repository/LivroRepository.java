package br.ifrn.aula.livraria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ifrn.aula.livraria.model.Livro;

@Repository
public interface LivroRepository extends JpaRepository<Livro,Long> {

}

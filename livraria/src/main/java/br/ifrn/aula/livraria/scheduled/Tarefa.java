package br.ifrn.aula.livraria.scheduled;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Tarefa {
	private final long SEGUNDO = 1000;
	private final long MINUTO = SEGUNDO * 60;
	private final long HORA = MINUTO * 60;
	private static final String TIME_ZONE = "America/Sao_Paulo";
	
	@Scheduled(initialDelay= MINUTO*2 , fixedDelay = MINUTO)
	public void verificaPorHora() {
		System.out.println( "Testando Agendamento" );
	}
	
	@Scheduled(cron = "0 */2  * * * *", zone = TIME_ZONE)
	public void utilizancoCon() {
		System.out.println( "Utilizando CRON" );
	}
}


package br.ifrn.aula.livraria.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.ifrn.aula.livraria.model.Autor;
import br.ifrn.aula.livraria.model.Livro;
import br.ifrn.aula.livraria.service.AutorService;
import br.ifrn.aula.livraria.service.LivroService;

@Controller
@RequestMapping("/livro")
public class LivroController {
	
	@Autowired
    private LivroService service;
	
	@Autowired
	private AutorService autorService;
	
	
	@RequestMapping("/add")
	public ModelAndView add(Livro livro) {
		ModelAndView mv = new ModelAndView("livros/form");
		mv.addObject("livro", livro);
		mv.addObject("autores",autorService.findAll());
		return mv;
		
	}
	
	@GetMapping("/edit/{id}")
	private ModelAndView edit( @PathVariable("id") Long id) {
		Livro livro = service.findOne(id);
		return add(livro);
	}
	
	@PostMapping("/save")
    public ModelAndView save(@Valid Livro livro, BindingResult result) {
         
        if(result.hasErrors()) {
            return add(livro);
        }
         
        service.save(livro);
         
        return findAll();
    }
	
	@GetMapping("/lista")
	private ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("/livros/lista");
        mv.addObject("livros", service.findAll());
        return mv;
	}
	
}

package br.ifrn.aula.livraria.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.ifrn.aula.livraria.model.User;

@Controller
public class IndexController {

	@GetMapping("/")
	public String login(@AuthenticationPrincipal User user) {
	    if(user != null) {
            return "redirect:/layout";
        }
		return "redirect:/entrar";
	}
	
	@RequestMapping(method=RequestMethod.POST,path= {"/layout"})
	public String index() {
		return "layout";
	}
	
	@RequestMapping(method=RequestMethod.GET,path= {"/layout"})
	public String index2() {
		return "layout";
	}
	
	@RequestMapping(method=RequestMethod.GET,path= {"/entrar"})
	public String entrar() {
		return "login";
	}
	
	
}

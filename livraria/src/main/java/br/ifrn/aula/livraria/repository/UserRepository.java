package br.ifrn.aula.livraria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ifrn.aula.livraria.model.User;

@Repository
public interface UserRepository  extends JpaRepository<User,Long>{

	User findByUsername(String username);
	
}

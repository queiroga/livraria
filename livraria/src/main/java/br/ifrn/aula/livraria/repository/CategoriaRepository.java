package br.ifrn.aula.livraria.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ifrn.aula.livraria.model.Categoria;

public interface CategoriaRepository extends  JpaRepository<Categoria,Long> {

}

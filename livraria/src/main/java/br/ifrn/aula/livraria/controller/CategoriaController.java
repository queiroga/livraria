package br.ifrn.aula.livraria.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.ifrn.aula.livraria.model.Autor;
import br.ifrn.aula.livraria.model.Categoria;
import br.ifrn.aula.livraria.service.CategoriaService;

@Controller
@RequestMapping("/categoria")
public class CategoriaController {
	
	@Autowired
	private CategoriaService service;
	
	@RequestMapping("/add")
	public ModelAndView add(Categoria cat) {
		ModelAndView mv = new ModelAndView("categoria/form");
		mv.addObject("categoria", cat);
		return mv;
	}
	
	@GetMapping("/edit/{id}")
	private ModelAndView edit( @PathVariable("id") Long id) {
		Categoria cat = service.findOne(id);
		return add(cat);
	}
	

	@GetMapping("/delete/{id}")
	private ModelAndView delete( @PathVariable("id") Long id) {
		service.delete(id);
		return findAll();
	}
	
	@GetMapping("/lista")
	public ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("categoria/lista");
        mv.addObject("categorias", service.findAll());
        return mv;
	}
	
	@PostMapping("/save")
    public ModelAndView save(@Valid Categoria cat, BindingResult result) {
         
        if(result.hasErrors()) {
            return add(cat);
        }
        service.save(cat);
         
        return findAll();
    }
}

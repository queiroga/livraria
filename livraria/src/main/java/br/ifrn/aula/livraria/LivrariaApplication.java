package br.ifrn.aula.livraria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

//Esse anotação componentScan servi para indicar quais os pacotes que o sprign
// deverá buscar classes @controlle e @repository


@SpringBootApplication
@ComponentScan(basePackages = {"br.ifrn.aula.livraria.controller", "br.ifrn.aula.livraria.service",
		"br.ifrn.aula.livraria.repository","br.ifrn.aula.livraria.config",
		"br.ifrn.aula.livraria.scheduled"})
@EnableCaching
@EnableScheduling
public class LivrariaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LivrariaApplication.class, args);
	}
	
	
	@Bean
    public CacheManager cacheManager(){
        return new ConcurrentMapCacheManager();
    }
}

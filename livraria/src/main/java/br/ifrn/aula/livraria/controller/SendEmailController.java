package br.ifrn.aula.livraria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.ifrn.aula.livraria.service.EmailServiceImpl;

@Controller
public class SendEmailController {
	
	@Autowired
	public EmailServiceImpl emaildMailComponent;
	
	@GetMapping("/serviceMail")
	public String sendMail() {
		emaildMailComponent.sendSimpleMessage("jeferson.queiroga@ifrn.edu.br", "testando", 
				"ola pessoal","jefersonqueiroga@gmail.com");
		return "index";
	}
}

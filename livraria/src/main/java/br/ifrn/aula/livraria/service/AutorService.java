package br.ifrn.aula.livraria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import br.ifrn.aula.livraria.model.Autor;
import br.ifrn.aula.livraria.repository.AutorRepository;

@Service
public class AutorService {

	@Autowired
	private AutorRepository repository;
	
	@Cacheable(value="cacheAutor")
	public  List<Autor> findAll() {
		return repository.findAll();
	}
	
	public Autor findOne(Long id) {
        return repository.getOne(id);
    }
     
	@CacheEvict(value="cacheAutor", allEntries=true)
    public Autor save(Autor autor) {
        return repository.saveAndFlush(autor);
    }
     
    public void delete(Long id) {
        repository.deleteById(id);
    }
    
	
}

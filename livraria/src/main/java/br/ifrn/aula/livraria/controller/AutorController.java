package br.ifrn.aula.livraria.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.ifrn.aula.livraria.model.Autor;
import br.ifrn.aula.livraria.service.AutorService;

@Controller
@RequestMapping("/autor")
public class AutorController {

	
	@Autowired
	private AutorService service;

	@RequestMapping("/add")
	public ModelAndView add(Autor autor) {
		ModelAndView mv = new ModelAndView("autor/form");
		mv.addObject("autor", autor);
			
		return mv;
	}
	
	@GetMapping("/edit/{id}")
	private ModelAndView edit( @PathVariable("id") Long id) {
		
		System.out.println("id:" + id);
		Autor autor = service.findOne(id);
				
		return add(autor);
	}
	

	@GetMapping("/delete/{id}")
	private ModelAndView delete( @PathVariable("id") Long id) {
		service.delete(id);
		return findAll();
	}
	
	@GetMapping("/lista")
	public ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("autor/lista");
        mv.addObject("autores", service.findAll());
        return mv;
	}
	
	@PostMapping("/save")
    public ModelAndView save(@Valid Autor autor, @RequestParam("uploadfile") MultipartFile file , BindingResult result) throws IOException {
         
        if(result.hasErrors()) {
            return add(autor);
        }
       
        autor.setImagem(file.getBytes());
        service.save(autor);
         
        return findAll();
    }
	
	
	@RequestMapping(value = "/image/{image_id}", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable("image_id") Long imageId) throws IOException {

    	Autor autor = service.findOne(imageId);
        byte[] imageContent = autor.getImagem();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<byte[]>(imageContent, headers, HttpStatus.OK);
    }
    
    
   
}

























package br.ifrn.aula.livraria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ifrn.aula.livraria.model.Livro;
import br.ifrn.aula.livraria.repository.LivroRepository;

@Service
public class LivroService {
	
	@Autowired
	private LivroRepository repository;
	
	public  List<Livro> findAll() {
		return repository.findAll();
	}
	
	public Livro findOne(Long id) {
        return repository.getOne(id);
    }
     
    public Livro save(Livro livro) {
        return repository.saveAndFlush(livro);
    }
     
    public void delete(Long id) {
        repository.deleteById(id);
    }
	
	
}
